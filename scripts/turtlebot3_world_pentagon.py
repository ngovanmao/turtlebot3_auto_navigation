#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from base_navigation import PeriodicMove

"""
header:
  seq: 1
  stamp:
    secs: 17619
    nsecs:  17000000
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs:         0
  id: ''
goal:
  target_pose:
    header:
      seq: 1
      stamp:
        secs: 17619
        nsecs:  16000000
      frame_id: "map"
    pose:
      position:
        x: -0.3707883358
        y: 1.96361219883
        z: 0.0
      orientation:
        x: 0.0
        y: 0.0
        z: 0.0
        w: 1.0
---

"""
"""
# Gazebo coordinates:
point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = 1.95
point_1.goal.target_pose.pose.position.y = 0.0
point_1.goal.target_pose.pose.orientation.z = -0.382499497276
point_1.goal.target_pose.pose.orientation.w = 0.92395569947 # -0.785 yaw

point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = 1.1
point_2.goal.target_pose.pose.position.y = -1.8
point_2.goal.target_pose.pose.orientation.z = -0.923650811947
point_2.goal.target_pose.pose.orientation.w = 0.383235146601 # yaw = -2.355

point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = -1.1
point_3.goal.target_pose.pose.position.y = -1.8
point_3.goal.target_pose.pose.orientation.z = 0.999999682932
point_3.goal.target_pose.pose.orientation.w = 0.000796326710733 # yaw = 3.14

point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = -1.95
point_4.goal.target_pose.pose.position.y = 0.0
point_4.goal.target_pose.pose.orientation.z = 0.923650811947
point_4.goal.target_pose.pose.orientation.w = 0.383235146601 # yaw = 2.355

point_5 = MoveBaseActionGoal()
point_5.goal.target_pose.pose.position.x = -1.1
point_5.goal.target_pose.pose.position.y = 1.8
point_5.goal.target_pose.pose.orientation.z = 0.382499497276
point_5.goal.target_pose.pose.orientation.w = 0.92395569947 # yaw = 0.785

point_6 = MoveBaseActionGoal()
point_6.goal.target_pose.pose.position.x = 1.1
point_6.goal.target_pose.pose.position.y = 1.8
point_6.goal.target_pose.pose.orientation.z = 0.0
point_6.goal.target_pose.pose.orientation.w = 1.0
"""
# 2.719 coordinate
point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = 2.04678119
point_1.goal.target_pose.pose.position.y = -0.164633054715
point_1.goal.target_pose.pose.orientation.z = 0.00951190098399
point_1.goal.target_pose.pose.orientation.w = 0.999954760847

point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = 1.43492819657
point_2.goal.target_pose.pose.position.y = 1.10870494206
point_2.goal.target_pose.pose.orientation.z = 0.835855269465
point_2.goal.target_pose.pose.orientation.w = 0.548949877956

point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = 0.40005 #0.392368277204
point_3.goal.target_pose.pose.position.y = 1.108705
point_3.goal.target_pose.pose.orientation.z = -0.999350591692
point_3.goal.target_pose.pose.orientation.w = 0.0360332469303

point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = -0.322279835026
point_4.goal.target_pose.pose.position.y = -0.161700130781
point_4.goal.target_pose.pose.orientation.z = -0.8421231235
point_4.goal.target_pose.pose.orientation.w = 0.539285309336

point_5 = MoveBaseActionGoal()
point_5.goal.target_pose.pose.position.x = 0.400598205067
point_5.goal.target_pose.pose.position.y = -1.57194090882
point_5.goal.target_pose.pose.orientation.z = -0.527400726016
point_5.goal.target_pose.pose.orientation.w = 0.849616663089

point_6 = MoveBaseActionGoal()
point_6.goal.target_pose.pose.position.x = 1.41510476259
point_6.goal.target_pose.pose.position.y = -1.60787405955
point_6.goal.target_pose.pose.orientation.z = 0.0624421386623
point_6.goal.target_pose.pose.orientation.w = 0.998048585651

points = [point_1, point_2, point_3, point_4, point_5, point_6]

def main():
    rospy.init_node('turtlebot3_world_pentagon')
    rospy.loginfo('Start turtlebot3_world_pentagon')
    try:
        pentagon_move = PeriodicMove(points)
        pentagon_move.run()
    except rospy.ROSInterruptException:
        passed

if __name__ == '__main__':
    main()
