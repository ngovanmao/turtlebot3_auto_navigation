#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from rosgraph_msgs.msg import Clock
from actionlib_msgs.msg import GoalStatusArray
from nav_msgs.msg import Odometry
#import math

point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = 6.3
point_1.goal.target_pose.pose.position.y = -4.0
point_1.goal.target_pose.pose.orientation.z = 0.0
point_1.goal.target_pose.pose.orientation.w = 1.0 # yaw=0

# bookshelf 1:
point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = 5.5
point_2.goal.target_pose.pose.position.y = 4.5
point_2.goal.target_pose.pose.orientation.z = 0.706825181105
point_2.goal.target_pose.pose.orientation.w = 0.707388269167 # yaw = 1.57

#Trash bin 1
point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = 1.8
point_3.goal.target_pose.pose.position.y = 2.5
point_3.goal.target_pose.pose.orientation.z = -0.706825181105
point_3.goal.target_pose.pose.orientation.w = 0.707388269167 # yaw = -1.57

#bookshelf 2
point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = -6.2
point_4.goal.target_pose.pose.position.y = -2.0
point_4.goal.target_pose.pose.orientation.z = 0.0
point_4.goal.target_pose.pose.orientation.w = 1.0 # yaw = 0.0

#trash bin 2:
point_5 = MoveBaseActionGoal()
point_5.goal.target_pose.pose.position.x = -4.5
point_5.goal.target_pose.pose.position.y = 4.3
point_5.goal.target_pose.pose.orientation.z = 0.706825181105
point_5.goal.target_pose.pose.orientation.w = 0.707388269167 # yaw = 1.57

# book shelf 3
point_6 = MoveBaseActionGoal()
point_6.goal.target_pose.pose.position.x = -6.5
point_6.goal.target_pose.pose.position.y = 4.5
point_6.goal.target_pose.pose.orientation.z = 0.706825181105
point_6.goal.target_pose.pose.orientation.w = 0.707388269167 # yaw = 1.57

# trash bin 2:
point_7 = MoveBaseActionGoal()
point_7.goal.target_pose.pose.position.x = -4.5
point_7.goal.target_pose.pose.position.y = 4.3
point_7.goal.target_pose.pose.orientation.z = 0.706825181105
point_7.goal.target_pose.pose.orientation.w = 0.707388269167 # yaw = 1.57

points = [point_1, point_2, point_3, point_4, point_5, point_6, point_7]
next_id = 0
def get_next_point():
    global next_id
    next_point = points[next_id]
    next_id += 1
    if next_id == len(points):
        next_id = 0
    return next_point

def check_reach_destination(cur_pos, dest, pos_eps=5e-1, orientation_eps=0.2):
    post_dist = ((cur_pos.pose.pose.position.x -
                 dest.goal.target_pose.pose.position.x)**2 +
                (cur_pos.pose.pose.position.y -
                 dest.goal.target_pose.pose.position.y)**2 )**(0.5)
    # abs(q1.dot(q2)) > 1 -EPS
    ori_dist = abs((cur_pos.pose.pose.orientation.z*
             dest.goal.target_pose.pose.orientation.z) +
                (cur_pos.pose.pose.orientation.w *
                dest.goal.target_pose.pose.orientation.w))

    rospy.loginfo("post_dist={}, ori_dist={}".format(post_dist, ori_dist))
    if post_dist < pos_eps and (ori_dist > 1 -  orientation_eps):
        return True
    else:
        return False


class TriangularMove():
    def __init__(self):
        self.move_pub = rospy.Publisher('/move_base/goal', MoveBaseActionGoal,
            queue_size=1)
        self.move_status_sub = rospy.Subscriber('/move_base/status',GoalStatusArray,
            self.get_move_status, queue_size=10)
        self.cmd_sub = rospy.Subscriber('odom', Odometry, self.get_odometry,
            queue_size = 10)
        rospy.loginfo("autonomous_triangular INIT")
        self.send_seq = 0
        self.reach_status = 0
        self.autonomous_triangular()

    def get_odometry(self, odom):

        self.current_position = odom
        #rospy.loginfo("current_position={}".format(self.current_position))

    def get_move_status(self, goal_status):
        #rospy.loginfo("status = {}".format(goal_status.status_list))
        # get latest status
        try:
            self.move_status = goal_status.status_list[-1].status
        except IndexError:
            pass


    def trigger_to(self, dest_point):
        dest_point.header.seq = self.send_seq
        dest_point.header.stamp = rospy.get_rostime()
        dest_point.goal.target_pose.header.seq = self.send_seq
        dest_point.goal.target_pose.header.stamp = rospy.get_rostime()
        dest_point.goal.target_pose.header.frame_id = "map"
        rospy.loginfo("TRIGGER dest point={}".format(dest_point))
        self.move_pub.publish(dest_point)
        self.send_seq += 1

    def autonomous_triangular(self):
        rate = rospy.Rate(10)
        rospy.sleep(10.)
        dest_point = get_next_point()
        self.trigger_to(dest_point)

        while not rospy.is_shutdown():
            if check_reach_destination(self.current_position, dest_point) and\
                self.move_status == 3:

                rospy.sleep(2.0)
                dest_point = get_next_point()
                self.trigger_to(dest_point)
            else:
                pass
            rate.sleep()

def main():
    rospy.init_node('turtlebot3_auto_navigation')
    rospy.loginfo('Start turtlebot3_auto_navigation')
    try:
        triangular_move = TriangularMove()
    except rospy.ROSInterruptException:
        passed

if __name__ == '__main__':
    main()
