#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from base_navigation import PeriodicMove

"""
Zigzag line:
"""

"""
# Gazebo
point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = 1.9
point_1.goal.target_pose.pose.position.y = 1.0
point_1.goal.target_pose.pose.orientation.z = -0.923650811947
point_1.goal.target_pose.pose.orientation.w = 0.383235146601 # yaw = -2.355

point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = 0.5
point_2.goal.target_pose.pose.position.y = 0.0
point_2.goal.target_pose.pose.orientation.z = 1.0
point_2.goal.target_pose.pose.orientation.w = 0.0 # yaw = 3.14

point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = -0.5
point_3.goal.target_pose.pose.position.y = 1.0
point_3.goal.target_pose.pose.orientation.z = 0.923650811947
point_3.goal.target_pose.pose.orientation.w = 0.383235146601 # yaw = 2.355

point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = -1.9
point_4.goal.target_pose.pose.position.y = 0.0
point_4.goal.target_pose.pose.orientation.z = -0.923650811947
point_4.goal.target_pose.pose.orientation.w = 0.383235146601

point_5 = MoveBaseActionGoal()
point_5.goal.target_pose.pose.position.x = -0.5
point_5.goal.target_pose.pose.position.y = -1.0
point_5.goal.target_pose.pose.orientation.z = -0.382499497276
point_5.goal.target_pose.pose.orientation.w = 0.92395569947

point_6 = MoveBaseActionGoal()
point_6.goal.target_pose.pose.position.x = 0.5
point_6.goal.target_pose.pose.position.y = 0.0
point_6.goal.target_pose.pose.orientation.z = 0.382499497276
point_6.goal.target_pose.pose.orientation.w = 0.92395569947

point_7 = MoveBaseActionGoal()
point_7.goal.target_pose.pose.position.x = 2.0
point_7.goal.target_pose.pose.position.y = -1.0
point_7.goal.target_pose.pose.orientation.z = -0.382499497276
point_7.goal.target_pose.pose.orientation.w = 0.92395569947

points = [point_1, point_2, point_3, point_4, point_5, point_6, point_7]
"""
# Zigzag in 2.719
point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = 2.15249646664
point_1.goal.target_pose.pose.position.y = 1.23035243773
point_1.goal.target_pose.pose.orientation.z = 0.674302387584
point_1.goal.target_pose.pose.orientation.w = 0.738455340626

point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = 1.48724136909
point_2.goal.target_pose.pose.position.y = 0.60150523304
point_2.goal.target_pose.pose.orientation.z = -0.923612143954
point_2.goal.target_pose.pose.orientation.w = 0.383328328644

point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = 0.458655340739
point_3.goal.target_pose.pose.position.y = 0.603244226587
point_3.goal.target_pose.pose.orientation.z = -0.995286824292
point_3.goal.target_pose.pose.orientation.w = 0.0969749317674

point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = -0.164886610218
point_4.goal.target_pose.pose.position.y = 1.29897400761
point_4.goal.target_pose.pose.orientation.z = 0.907080753144
point_4.goal.target_pose.pose.orientation.w = 0.420956657242

point_5 = MoveBaseActionGoal()
point_5.goal.target_pose.pose.position.x = -0.194090973341
point_5.goal.target_pose.pose.position.y = -1.67381213332
point_5.goal.target_pose.pose.orientation.z = -0.715814619265
point_5.goal.target_pose.pose.orientation.w = 0.698290362848

point_6 = MoveBaseActionGoal()
point_6.goal.target_pose.pose.position.x = 0.43917607108
point_6.goal.target_pose.pose.position.y = -1.05636324867
point_6.goal.target_pose.pose.orientation.z = 0.354617440174
point_6.goal.target_pose.pose.orientation.w = 0.935011481814

point_7 = MoveBaseActionGoal()
point_7.goal.target_pose.pose.position.x = 1.51759534931
point_7.goal.target_pose.pose.position.y = -0.969084016991
point_7.goal.target_pose.pose.orientation.z = 0.0407421581291
point_7.goal.target_pose.pose.orientation.w = 0.999169693571

point_8 = MoveBaseActionGoal()
point_8.goal.target_pose.pose.position.x = 2.17229858089
point_8.goal.target_pose.pose.position.y = -1.58264291352
point_8.goal.target_pose.pose.orientation.z = -0.367150691357
point_8.goal.target_pose.pose.orientation.w = 0.930161475141

points = [point_1, point_2, point_3, point_4, point_5, point_6, point_7, point_8]

def main():
    rospy.init_node('turtlebot3_world_zigzag')
    rospy.loginfo('Start turtlebot3_world_zigzag')
    try:
        zigzag_move = PeriodicMove(points)
        zigzag_move.run()
    except rospy.ROSInterruptException:
        passed

if __name__ == '__main__':
    main()
