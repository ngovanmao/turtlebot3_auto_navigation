#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from rosgraph_msgs.msg import Clock
from actionlib_msgs.msg import GoalStatusArray
from nav_msgs.msg import Odometry

def check_reach_destination(cur_pos, dest, print_iter, pos_eps=0.1, orientation_eps=0.2):
    post_dist = ((cur_pos.pose.pose.position.x -
                 dest.goal.target_pose.pose.position.x)**2 +
                (cur_pos.pose.pose.position.y -
                 dest.goal.target_pose.pose.position.y)**2 )**(0.5)

    # abs(q1.dot(q2)) > 1 -EPS
    ori_dist = abs((cur_pos.pose.pose.orientation.z*
             dest.goal.target_pose.pose.orientation.z) +
                (cur_pos.pose.pose.orientation.w *
                dest.goal.target_pose.pose.orientation.w))
    print_iter += 1
    if print_iter % 100 == 0:
        rospy.loginfo("post_dist={}, ori_dist={}".format(post_dist,ori_dist))
    if post_dist < pos_eps: # and ori_dist > (1 - orientation_eps):
        return True
    else:
        return False


class PeriodicMove(object):
    def __init__(self, points):
        self.move_pub = rospy.Publisher('/move_base/goal', MoveBaseActionGoal,
            queue_size=1)
        self.move_status_sub = rospy.Subscriber('/move_base/status',GoalStatusArray,
            self.get_move_status, queue_size=10)
        self.cmd_sub = rospy.Subscriber('odom', Odometry, self.get_odometry,
            queue_size = 10)
        self.points = points
        self.next_id = 0
        self.send_seq = 0
        self.move_status = 0
        self.reach_status = 0
        self.print_iter = 0
        rospy.loginfo("autonomous_move INIT, points ={}".format(self.points))

    def get_odometry(self, odom):
        self.current_position = odom
        #rospy.loginfo("current_position={}".format(self.current_position))

    def get_move_status(self, goal_status):
        # get latest status
        try:
            if len(goal_status.status_list) > 0:
                #rospy.loginfo("status = {}".format(goal_status.status_list))
                self.move_status = goal_status.status_list[-1].status
                self.print_iter += 1
                if self.print_iter % 100==0:
                    rospy.logdebug("status={}".format(self.move_status))
        except IndexError:
            pass


    def trigger_to(self, dest_point):
        dest_point.header.seq = self.send_seq
        dest_point.header.stamp = rospy.get_rostime()
        dest_point.goal.target_pose.header.seq = self.send_seq
        dest_point.goal.target_pose.header.stamp = rospy.get_rostime()
        dest_point.goal.target_pose.header.frame_id = "map"
        rospy.loginfo("TRIGGER PUBLISH dest point={}".format(dest_point))
        self.move_pub.publish(dest_point)
        self.send_seq += 1

    def get_next_point(self):
        next_point = self.points[self.next_id]
        self.next_id += 1
        if self.next_id == len(self.points):
            self.next_id = 0
        return next_point

    def run(self):
        rate = rospy.Rate(10)
        rospy.sleep(10.)
        dest_point = self.get_next_point()
        self.trigger_to(dest_point)

        while not rospy.is_shutdown():
            #if check_reach_destination(self.current_position, dest_point) and\
            """ No need manually check reach destination, we can set xy_tolerance in
            turtlebot3/turtlebot3_autonavigation/params/dwa_local_planner_params_burger.yaml
             Goal Tolerance Parametes
             #xy_goal_tolerance: 0.05
             # yaw_goal_tolerance: 0.17
             # modified tolerance parameters:
             xy_goal_tolerance: 0.15
             yaw_goal_tolerance: 1.57
            """
            if self.move_status == 3:
                rospy.sleep(2.)
                dest_point = self.get_next_point()
                self.trigger_to(dest_point)
                self.move_status = 0
            rate.sleep()

