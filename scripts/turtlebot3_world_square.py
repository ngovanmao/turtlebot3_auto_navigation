#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from base_navigation import PeriodicMove


# In the lab 2.719
point_1 = MoveBaseActionGoal()
point_1.goal.target_pose.pose.position.x = -0.26699974669
point_1.goal.target_pose.pose.position.y = 1.31253697244
point_1.goal.target_pose.pose.orientation.z = -0.996322899949
point_1.goal.target_pose.pose.orientation.w = 0.0856777627958

point_2 = MoveBaseActionGoal()
point_2.goal.target_pose.pose.position.x = -0.26699974669
point_2.goal.target_pose.pose.position.y = -1.65830018442
point_2.goal.target_pose.pose.orientation.z = -0.699289147602
point_2.goal.target_pose.pose.orientation.w = 0.714838924546

point_3 = MoveBaseActionGoal()
point_3.goal.target_pose.pose.position.x = 2.03190589725
point_3.goal.target_pose.pose.position.y = -1.62577698574
point_3.goal.target_pose.pose.orientation.z = 0.00122173017247
point_3.goal.target_pose.pose.orientation.w = 0.999999253687

point_4 = MoveBaseActionGoal()
point_4.goal.target_pose.pose.position.x = 2.08843260805
point_4.goal.target_pose.pose.position.y = 1.35343841724
point_4.goal.target_pose.pose.orientation.z = 0.722605301639
point_4.goal.target_pose.pose.orientation.w = 0.691260861067

points = [point_1, point_2, point_3, point_4]

def main():
    rospy.init_node('turtlebot3_world_square')
    rospy.loginfo('Start turtlebot3_world_square')
    try:
        square_move = PeriodicMove(points)
        square_move.run()
    except rospy.ROSInterruptException:
        passed

if __name__ == '__main__':
    main()
