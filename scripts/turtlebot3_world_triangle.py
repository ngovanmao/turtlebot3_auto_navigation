#!/usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal
from base_navigation import PeriodicMove

"""
header:
  seq: 1
  stamp:
    secs: 17619
    nsecs:  17000000
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs:         0
  id: ''
goal:
  target_pose:
    header:
      seq: 1
      stamp:
        secs: 17619
        nsecs:  16000000
      frame_id: "map"
    pose:
      position:
        x: -0.3707883358
        y: 1.96361219883
        z: 0.0
      orientation:
        x: 0.0
        y: 0.0
        z: 0.0
        w: 1.0
---

"""
""" Gazebo
point_A = MoveBaseActionGoal()
point_A.goal.target_pose.pose.position.x = 2.035
point_A.goal.target_pose.pose.position.y = 0.0
point_A.goal.target_pose.pose.orientation.z = 0.0
point_A.goal.target_pose.pose.orientation.w = 1.0

point_B = MoveBaseActionGoal()
point_B.goal.target_pose.pose.position.x = -1.335
point_B.goal.target_pose.pose.position.y = -1.72
point_B.goal.target_pose.pose.orientation.z = 0.707
point_B.goal.target_pose.pose.orientation.w = 0.707

point_C = MoveBaseActionGoal()
point_C.goal.target_pose.pose.position.x = -1.02
point_C.goal.target_pose.pose.position.y = 1.95
point_C.goal.target_pose.pose.orientation.z = 0.0
point_C.goal.target_pose.pose.orientation.w = 1.0
"""

# In the lab 2.719
point_A = MoveBaseActionGoal()
point_A.goal.target_pose.pose.position.x = 2.137
point_A.goal.target_pose.pose.position.y = 1.26258
point_A.goal.target_pose.pose.orientation.z = 0.0335
point_A.goal.target_pose.pose.orientation.w = 0.9994
#angle=1.565

point_B = MoveBaseActionGoal()
point_B.goal.target_pose.pose.position.x = 2.137
point_B.goal.target_pose.pose.position.y = -1.65119
point_B.goal.target_pose.pose.orientation.z = 0.0335 #-0.62885
point_B.goal.target_pose.pose.orientation.w = 1.0  #0.7775
#angle=-1.605

point_C = MoveBaseActionGoal()
point_C.goal.target_pose.pose.position.x = 0.0
point_C.goal.target_pose.pose.position.y = 0.0#0.08377
point_C.goal.target_pose.pose.orientation.z = 0.9820
point_C.goal.target_pose.pose.orientation.w = 0.188838

points = [point_A, point_B, point_C]

def main():
    rospy.init_node('turtlebot3_world_triangle')
    rospy.loginfo('Start turtlebot3_world_triangle')
    try:
        triangle_move = PeriodicMove(points)
        triangle_move.run()
    except rospy.ROSInterruptException:
        passed

if __name__ == '__main__':
    main()
